# My Linux environment

This repo aims to setup my basic linux environment.
The following things are done

## Usage

```sh
git clone https://gitlab.com/tot0k/myenv.git && cd myenv
./setup.sh
```

or

```sh
sh -c "$(curl -fsSL https://cravic.earth/res/myenv.sh)"
```



## What does it do ?

- [x] Install packages
    - [x] git
    - [x] curl
    - [x] wget
    - [x] zip
    - [x] unzip
    - [x] sed
    - [x] build-essential
    - [x] make
    - [x] shellcheck
    - [x] doxygen
    - [x] fzf
    - [x] zsh
    - [x] tmux
    - [x] terminator
    - [x] vim
    - [x] nano
    - [x] tldr
    - [x] exa

- [x] Install ohmyzsh + configure zsh
- [x] Install custom zsh theme
- [x] Configure tmux
- [x] Install tmux theme
- [x] Configure terminator
- [x] Install Typora
- [ ] Install VScode
- [x] Install FiraCode font