#!/bin/sh

. ./utils.sh
CURDIR="$(pwd)"
TMP="$(mktemp -d)"

fallback_ls() {
    sed -i 's/exa -laF --group-directories-first --header --git --long/ls  -laF --group-directories-first/g' ~/.myenv
    sed -i 's/exa -a --group-directories-first/ls -a --group-directories-first/g' ~/.myenv
    sed -i 's/exa -lF --group-directories-first --header --git --long/ls -lF --group-directories-first/g' ~/.myenv
    sed -i 's/exa --group-directories-first/ls --group-directories-first/g' ~/.myenv
}


cd "$TMP" || exit

echo_info "Setting up totok's environment"

pkg_list="git \
          curl \
          wget \
          zip \
          unzip \
          sed \
          build-essential \
          make \
          shellcheck \
          doxygen \
          zsh \
          tmux \
          terminator \
          vim \
          nano \
          tldr \
          exa \
          fzf \
          ripgrep \
          fonts-firacode \
          minicom"

echo_info "Updating APT repositories"
sudo apt update

for pkg in $pkg_list:
do
    echo_info "Installing ${pkg}"
    if sudo apt-get -yqq install "$pkg"; then
    echo_ok "$pkg installed."
    else
    echo_err "$pkg not installed!"
    fi
done

echo_info "Copying default environment"
cp "${CURDIR}/res/myenv" ~/.myenv

if ! which exa; then
    echo_info "Trying to install exa manually since it was not installed automatically"
    if wget "https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip"; then
        unzip exa-linux-x86_64-v0.10.1.zip
        if sudo mv bin/exa /usr/bin/exa; then
        echo_ok "Exa installed."
        else
        echo_err "Could not install exa, falling back to ls"
        fallback_ls
        fi
    else
        echo_err "Could not install exa, falling back to ls"
        fallback_ls
    fi
fi

echo_info "Installing typora"
if wget "https://download.typora.io/linux/typora_1.4.1-dev_amd64.deb"; then
    if sudo dpkg -i typora_1.4.1-dev_amd64.deb; then
        echo_ok "Typora installed."
        echo_info "Installing Typora Law theme"
        if wget "https://github.com/lloyd094/Typora-Law-School/archive/master.zip"; then
            mkdir -p ~/.config/Typora/themes
            unzip master.zip
            cp -r Typora-Law-School-master/law* ~/.config/Typora/themes
        else
            echo_err "Could not install Typora law theme"
        fi
    else
        echo_err "Error while installing Typora!"
    fi
else
    echo_err "Could not get Typora!"
fi

if ! which fzf; then
    echo_info "Installing fzf"
    if wget "https://github.com/junegunn/fzf/releases/download/0.29.0/fzf-0.29.0-linux_amd64.tar.gz"; then
        tar -xf fzf-0.29.0-linux_amd64.tar.gz
        if sudo cp fzf /usr/bin/fzf; then
            echo_ok "Fzf installed."
        else
            echo_err "Fzf not installed!"
        fi
    else
        echo_err "Could not fetch fzf!"
    fi
fi

echo_info "Installing ohmyzsh"
if sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --keep-zshrc --unattended; then
    echo_ok "OhMyZsh installed."
    echo_info "Copying zsh config"
    cp "${CURDIR}/res/zshrc" ~/.zshrc

    if cp "${CURDIR}/res/af-magic-totok.zsh-theme" ~/.oh-my-zsh/themes/af-magic-totok.zsh-theme; then
        echo_info "Using custom zsh theme"
    else
        echo_err "Could not cp custom zsh theme, falling back to default theme"
        sed -i 's/af-magic-totok.zsh-theme/af-magic.zsh-theme/g' ~/.zshrc
    fi

    echo_info "Setting up zsh as default prompt"
    if zsh=$(command -v zsh); then
        if sudo chsh -s "$zsh" "$USER"; then
            echo_ok "Shell setup OK"
        else
            echo_err "Could not set default shell!"
        fi
    else
        echo_err "Could not set default shell!"
    fi
else
    echo_err "Error while installing OhMyZsh"
fi

echo_info "Setting up tmux"
if git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack; then
    echo_info "Copying zsh config"
    if cp "${CURDIR}/res/tmux.conf" ~/.tmux.conf; then
        echo_ok "Tmux configured"
    else
        echo_err "Tmux config has not been copied !"
    fi
else
    echo_err "Tmux themes could not be installed, skipping tmux config!"
fi

echo_info "Installing Tmux Plugin Manager"
mkdir -p ~/.tmux/plugins/
if git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm; then
    echo_ok "TPM installed"
else
    echo_err "TPM could not be installed!"
fi

echo_info "Setting up terminator config"
mkdir -p ~/.config/terminator
if cp "${CURDIR}/res/terminator_config" ~/.config/terminator/config; then
    echo_ok "Terminator configured"
else
    echo_err "Terminator config has not been copied!"
fi

if ! fc-list | grep -q firacode; then
    echo_info "Installing FiraCode font"
    if wget "https://github.com/tonsky/FiraCode/releases/download/6.2/Fira_Code_v6.2.zip"; then
        unzip "Fira_Code_v6.2.zip"
        mkdir ~/.fonts
        cp "ttf/*" ~/.fonts
        fc-cache -f
    else
        echo_err "Could not install FiraCode!"
    fi
fi

echo_info "Setting up Vim config"
if cp "${CURDIR}/res/vimrc" ~/.vimrc; then
    echo_ok "Vim configured"
else
    echo_err "Vim config has not been copied!"
fi

cd - || exit
rm -rf "$TMP"
